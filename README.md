# HsIndex

**HsIndex** is an index generator for **LaTeX**, **XeLaTeX** written with **Haskell**.

It parse **imakeidx** *.idx files and generate *.ind LaTeX index files.

This an alternative to **xindy**.