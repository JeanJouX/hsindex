-- |
-- Module      :  HsIndex.CharLists.German
-- Copyright   :  Jean-Luc JOULIN 2018-2019
-- License     :  General Public Licence (GPLv3)
-- Maintainer  :  Jean-Luc JOULIN  <jean-luc-joulin@orange.fr>
-- Stability   :  alpha
-- Portability :  portable
-- The letters, numbers and symbol definition for the german language.


module HsIndex.CharLists.German where


import           HsIndex.Functions
import           HsIndex.Types
import           HsIndex.CharLists.Symbols
import           HsIndex.CharLists.French       ( lstLatinLetter )



ordGerman = lstLatinLetter

ordGermanLowerUpper = lowerUpper lstLatinLetter

lstGermanLetter =
  [ 'a'
  , 'b'
  , 'c'
  , 'd'
  , 'e'
  , 'f'
  , 'g'
  , 'h'
  , 'i'
  , 'j'
  , 'k'
  , 'l'
  , 'm'
  , 'n'
  , 'o'
  , 'p'
  , 'q'
  , 'r'
  , 's'
  , 'ß'
  , 't'
  , 'u'
  , 'v'
  , 'w'
  , 'x'
  , 'y'
  , 'z'
  ]


charSubsGerman =
  [ ('æ', "ae")
  , ('ä', "a")
  , ('ö', "o")
  , ('ß', "ss")
  , ('ü', "u")
  ]




langDefGerman = LangDef
  { lstLetters  = lstSpace ++ ordGermanLowerUpper  -- ++ map substituted charSubsGerman
  , lstNumbers  = lstDigit
  , lstSymbols  = Nothing
  , lstSubs     = charSubsGerman
  , lstSecOrder = [Symbols, Letters, Numbers]
  }



