-- |
-- Module      :  HsIndex.CharLists.Symbols
-- Copyright   :  Jean-Luc JOULIN 2018-2019
-- License     :  General Public Licence (GPLv3)
-- Maintainer  :  Jean-Luc JOULIN  <jean-luc-joulin@orange.fr>
-- Stability   :  alpha
-- Portability :  portable
-- The list of symbols for all languages.


module HsIndex.CharLists.Symbols where


-- | Allowed symbols.
allowedSymb = " &~\"'()[]{}<>*+/`^#@°=$£µ%§:;.,?"

-- | Forbidden symbols.
--
-- These symbols
forbiddenSymb =  "!|\n\r" -- "!|{}\n\r"

-- | The hyphens symbols.
lstHyph = "-_"

-- | List of digits.
lstDigit = "0123456789"

lstSpace = " "


