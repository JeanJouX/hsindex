-- |
-- Module      :  HsIndex.CharLists.French
-- Copyright   :  Jean-Luc JOULIN 2018-2019
-- License     :  General Public Licence (GPLv3)
-- Maintainer  :  Jean-Luc JOULIN  <jean-luc-joulin@orange.fr>
-- Stability   :  alpha
-- Portability :  portable
-- The letters, numbers and symbol definition for the french language.


module HsIndex.CharLists.French where

import           HsIndex.Functions
import           HsIndex.CharLists.Symbols
import           HsIndex.Types

lstSubsLatin = take 1


ordFrench = lstLatinLetter

ordFrenchUpperLower = upperLower lstLatinLetter




lstLatinLetter =
  [ 'a'
  , 'b'
  , 'c'
  , 'd'
  , 'e'
  , 'f'
  , 'g'
  , 'h'
  , 'i'
  , 'j'
  , 'k'
  , 'l'
  , 'm'
  , 'n'
  , 'o'
  , 'p'
  , 'q'
  , 'r'
  , 's'
  , 't'
  , 'u'
  , 'v'
  , 'w'
  , 'x'
  , 'y'
  , 'z'
  ]


subsFrenchUpperLower = replaUpperLower subsFrench




subsFrench =
  [ ('à', "a")
  , ('â', "a")
  , ('å', "a")
  , ('ä', "a")
  
  , ('ç', "c")
  
  , ('é', "e")
  , ('è', "e")
  , ('ê', "e")
  , ('ë', "e")
  
  , ('î', "i")
  , ('ï', "i")
  
  , ('ô', "o")
  , ('ö', "o")
  
  , ('ù', "u")
  , ('û', "u")
  , ('ü', "u")
  
  
  , ('æ', "ae")
  , ('œ', "oe")
  ]


subsSymb =
  [ ('-', " ")
  , ('_', " ")
  , ('\\', " ")
  ]



langDefFrench = LangDef
  { lstLetters  = lstSpace ++  ordFrenchUpperLower --  ++ (map substituted subsFrenchUpperLower)
  , lstNumbers  = lstDigit
  , lstSymbols  = Nothing
  , lstSubs     = subsFrenchUpperLower++subsSymb
  , lstSecOrder = [Symbols, Numbers, Letters]
  }



