-- |
-- Module      :  HsIndex.CharLists.English
-- Copyright   :  Jean-Luc JOULIN 2018-2019
-- License     :  General Public Licence (GPLv3)
-- Maintainer  :  Jean-Luc JOULIN  <jean-luc-joulin@orange.fr>
-- Stability   :  alpha
-- Portability :  portable
-- The letters, numbers and symbol definition for the english language.


module HsIndex.CharLists.English where

import           HsIndex.Types
import           HsIndex.Functions
import           HsIndex.CharLists.Symbols
import           HsIndex.CharLists.French       



ordEnglish = lstLatinLetter

ordEnglishUpperLower = upperLower lstLatinLetter


-- | 
langDefEnglish :: LangDef
langDefEnglish = LangDef
  { lstLetters  = lstSpace ++ ordEnglishUpperLower
  , lstNumbers  = lstDigit
  , lstSymbols  = Nothing
  , lstSubs     = subsFrenchUpperLower++subsSymb
  , lstSecOrder = [Symbols, Numbers, Letters]
  }

