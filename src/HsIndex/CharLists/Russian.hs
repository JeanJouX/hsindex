-- |
-- Module      :  HsIndex.CharLists.Russian
-- Copyright   :  Jean-Luc JOULIN 2018-2019
-- License     :  General Public Licence (GPLv3)
-- Maintainer  :  Jean-Luc JOULIN  <jean-luc-joulin@orange.fr>
-- Stability   :  alpha
-- Portability :  portable
-- The letters, numbers and symbol definition for the russian language.


module HsIndex.CharLists.Russian where

import           HsIndex.Functions
import           HsIndex.Types
import           HsIndex.CharLists.Symbols

ordRussian = lstCyrillic

ordRussianUpperLower = upperLower lstCyrillic

-- | List of cyrillic characters.
lstCyrillic =
  [ 'а'
  , 'б'
  , 'в'
  , 'г'
  , 'д'
  , 'е'
  , 'ё'
  , 'ж'
  , 'з'
  , 'и'
  , 'й'
  , 'к'
  , 'л'
  , 'м'
  , 'н'
  , 'о'
  , 'п'
  , 'р'
  , 'с'
  , 'т'
  , 'у'
  , 'ф'
  , 'х'
  , 'ц'
  , 'ч'
  , 'ш'
  , 'щ'
  , 'ъ'
  , 'ы'
  , 'ь'
  , 'э'
  , 'ю'
  , 'я'
  ]





lettersRussian = lstCyrillic

langDefRussian = LangDef
  { lstLetters  = lstSpace ++ ordRussianUpperLower
  , lstNumbers  = lstDigit
  , lstSymbols  = Nothing
  , lstSubs     = []
  , lstSecOrder = [Symbols, Numbers, Letters]
  }



